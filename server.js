var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var cors = require('cors');
app.use(cors());

var port = process.env.PORT || 5300; // used to create, sign, and verify tokens
var path = require('path');
app.use(bodyParser.json({ limit: '50mb', extended: true }))
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }))
app.use(express.static(__dirname + '/public'));
const multer = require('multer');
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/uploads/');
    },

    // By default, multer removes file extensions so let's add them back
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    }
});
app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
});

app.post('/', (req, res) => {
    console.log('Reached API')
    // 'profile_pic' is the name of our file input field in the HTML form
    let upload = multer({ storage: storage }).single('file');
  
    upload(req, res, function (err) {
        // req.file contains information of uploaded file
        // req.body contains information of text fields, if there were any
        console.log('err:', err)
        // Display uploaded image for user validation
        console.log('path:', req.file.path)
        var path = req.file.path
        path = path.replace('public', '')
        res.send(path);
    });
});

var server = app.listen(port);
console.log('Server running at http://localhost:' + port);
